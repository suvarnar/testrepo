provider "aws" {
  region = "us-west-2"
}


resource "aws_ami" "awsAmiEncrypted" {
  #ts:skip=AC_AWS_0005 need to skip this rule
  name                = "some-name"
  ebs_block_device {
    device_name = "dev-name"
    encrypted = "false"
  }
}

resource "aws_vpc" "my_vpc" {
  cidr_block = "172.16.0.0/16"

  tags = {
    Name = "tf-example"
  }
}
